Name: 

Different shaped rooms.

Left and right arrow keys that shows the furniture right in front of you. Then you take it and move it. 

Values that can change depending on difficulty:
 - Remaining Time
 - Minimum amount of items to be placed.
 - Reset penalty time.
 - 

Controls:
-Switch Button(Switch Furniture to next furniture and after you go through all the furniture you start from the first unplaced item)
-Reset Button
-Place Button
- Teleportation (For VR)
- Arrow keys movement. (Flat Screen)
- Mouse For moving Camera (Flat Screen)

Features:
	Furniture (Left and right arrow keys to select furniture.)
	Timer (sounds)
	Different shaped rooms    

Rules:
Use the furniture to fill rooms within the time limit without collider issues.
	If you fail go back to the beginning level.
	If you place it wrong there is no way to undo.
	Minimum num of different items that need to be placed. (Little items are at the end of the list)
	
	
Timer:
2 minutes at start.
3D clock at the top of the level showing the time.
Everytime you reset you lose an amount of time from the timer.

Sounds:
Start timer sounds, End of time Sound, Counter Sound (maybe final 10), Change level sound, Drop object sound, fail sound or for level reset, Complete sound.
Sound comment when two items are colliding. Different Sound comment when colliding with walls.
Commentary at start of level. (SOme random funny commentary)
Commentary at start about taking your time or you will face consequences.
Commentary: remember 
Sound for when you flip through the list items
Music playing in the background.



Items:
Will have colliders that define the distance between items and the walls.
Colliders will be to the sides and to the top. 

Levels:
Made up of a wood floor that covers the whole world.
Placement of the walls is what changes.

Easy - Square shape
Hard - Star Shape


Extra feature:
A screen shot of the rooms after the game is completed.
VR. Screenshot Sound.