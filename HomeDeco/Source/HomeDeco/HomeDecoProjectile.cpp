// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "HomeDecoProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SphereComponent.h"

AHomeDecoProjectile::AHomeDecoProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AHomeDecoProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;
	RootComponent->SetWorldScale3D(FVector(1.0f) * 0.3f);

	// Use a box as a simple collision representation
	CollisionBoundary = CreateDefaultSubobject<UBoxComponent>(TEXT("BoundaryBox"));
	CollisionComp->OnComponentHit.AddDynamic(this, &AHomeDecoProjectile::OnHit);

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	//ProjectileMovement->bRotationFollowsVelocity = true;
	//ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	//InitialLifeSpan = 3.0f;

	ProjectileScale = 5.0;
	PlacementScale = ProjectileScale * 25.f;
}

void AHomeDecoProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	AHomeDecoProjectile* Item = Cast<AHomeDecoProjectile>(OtherActor);
	APawn* Character = Cast<APawn>(OtherActor);
	if (!Character && !Item)
	{
		//UWorld* const World = GetWorld();
		RootComponent->SetWorldScale3D(FVector(1.0f) * ProjectileScale);
		RootComponent->AddLocalOffset(RootComponent->GetForwardVector() * -PlacementScale);
	}
	else if (!Character && Item)
	{
		//isOverlapping = true;
		UE_LOG(LogTemp, Warning, TEXT("Your Lost!!!"));
		UGameplayStatics::OpenLevel(this, "GGJ_Master", false);
	}

}

